import React, { useState } from 'react'
import { Typography, Paper, Avatar, Button, FormControl, Input, InputLabel } from '@material-ui/core'
import withStyles from '@material-ui/core/styles/withStyles'
import { Link, withRouter } from 'react-router-dom'
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { MultiSelectComponent  } from '@syncfusion/ej2-react-dropdowns';

import firebase from '../firebase'
import app from 'firebase/app'
import Select from 'react-select';
import { useDispatch } from 'react-redux';
import {  addBoard } from '../Actions/action';


const styles = theme => ({
	main: {
		width: 'auto',
		display: 'block', // Fix IE 11 issue.
		marginLeft: theme.spacing.unit * 3,
		marginRight: theme.spacing.unit * 3,
		[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
			width: 400,
			marginLeft: 'auto',
			marginRight: 'auto',
		},
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
	},
	avatar: {
		margin: theme.spacing.unit,
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing.unit,
	},
	submit: {
		marginTop: theme.spacing.unit * 3,
	},
})



function NewBoard(props) {
	const { classes } = props

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [mambers, setMambers] = useState([]);
	const dispatch = useDispatch();
	const [boards, setBoards] = React.useState([]);
    var user_id = firebase.getCurrentId()
  

  React.useEffect(() => {
    const fetchData = async () => {
      const db = app.firestore();
      const data = await db.collection("users_").get();
      setBoards(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
    };
    fetchData();
  }, []);

//console.log(boards);

let users = []
 //boards.each((board) => {
	//console.log(board)
	//users.push({label: board.name, value: board.id})
//})

 for (let i = 0, l = boards.length; i < l; i++) {
 	if(users.length != boards.length){
 	 boards.map((e) => 
 	 	//console.log(e)
 	 	users.push({label: e.name, value: e.id})
 	  )
 	}
  }

// let mam = []

//  for (let i = 0, l = boards.length; i < l; i++) {
//   if(mam.length != boards.length){
//    boards.map((e) => 
//     console.log(e)
//       // if(e.mambers == user.name){
//       // 	console.log(user.id)
//       // }
    
//     )
//   }
//   }

//console.log(users)
 //console.log(mambers)

  //const array = boards.map((e) => {name: e.name, value: e.id});
//console.log(array);
 //const sportsData1: string[] = array;

   const handleChange = (e) => {
   	
   	  console.log('========================handleChange=========1========')
   	  console.log(e)
   	  console.log('========================handleChange=========2========')
      //console.log(e)

      const sportsData: string[] = 
      e.map((ev) =>({label: ev.label, value: ev.value}) || ev)
      console.log(sportsData)

      // { e && e.map(item => {
      //     return <div key={item.value}>{item.label} || item </div>;
      //   })}

   	  // console.log('========================handleChange==========3=======')

      setMambers(sportsData);
      //setMambers(event.target.value);
      
    }
	

	function onCreate (name: name , description: description, mambers: mambers, user_id: user_id,dispatch)  {
	   try {
	     dispatch(addBoard(name: name , description: description, mambers: mambers, user_id: user_id)) 
	     props.history.replace('/boards')
	   } catch(error) {
		  alert(error.message)
		} 
     
	
    }

	// const onCreate = () => {
	          
	//     try {
	// 		const db = app.firestore();
			
	// 		 db.collection('Boards').add({
	// 		  name , description , mambers , user_id
	// 	    })
			
	// 		props.history.replace('/boards')
	// 		} catch(error) {
	// 			alert(error.message)
	// 		}
	// }
		
    return (
    
    	<main className={classes.main}>
			<Paper className={classes.paper}>
				<Avatar className={classes.avatar}>
					<AddCircleIcon />
				</Avatar>
				<Typography component="h1" variant="h5">
					Create Board
       			</Typography>
				<form className={classes.form} onSubmit={e => e.preventDefault() && false }>
					<FormControl margin="normal" required fullWidth>
						<InputLabel htmlFor="name">Board Name</InputLabel>
						<Input id="name" name="name" autoComplete="off" autoFocus value={name} onChange={e => setName(e.target.value)} />
					</FormControl>
					<FormControl margin="normal" required fullWidth>
						<InputLabel htmlFor="description">Board Description</InputLabel>
						<Input  name="description" type="text" autoComplete="off" value={description} onChange={e => setDescription(e.target.value)}  />
					</FormControl>
					
				 <Select
				 	 placeholder="mambers"
				 	 onChange={handleChange}
				 	 autoFocus
				 	 noOptionsMessage={() => 'no other mambers :('}
			        isMulti
			        isSearchable
			       options={users}
			      />


					<Button
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						onClick={() => onCreate(name: name ,description: description,mambers: mambers, user_id: user_id,dispatch)}
						
						className={classes.submit}>
						Save
          			</Button>

					<Button
						type="submit"
						fullWidth
						variant="contained"
						color="secondary"
						component={Link}
						to="/boards"
						className={classes.submit}>
						Cancel
          			</Button>
				</form>
			</Paper>
		</main>
    
      
  	)

  	
}

export default withRouter(withStyles(styles)(NewBoard)) 







