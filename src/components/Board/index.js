import React , {useState, useEffect}from 'react'
import {  Button } from '@material-ui/core'
import { Link } from 'react-router-dom'
import Modal from "react-bootstrap/Modal";
import "bootstrap/dist/css/bootstrap.min.css";
import firebase from '../firebase'
import NewBoard from '../Board/new.js'
import app from 'firebase/app'
import './index.css'

import { useDispatch } from 'react-redux';
import {deleteBoard } from '../Actions/action';




function Board(props) {
	const [boards, setBoards] = React.useState([]);
  const dispatch = useDispatch();

   if(!firebase.getCurrentUsername()) {
    // not logged in
    alert('Please login first')
    props.history.replace('/login')
    return null
  }
  

  React.useEffect(() => {
    const fetchData = async () => {
      const db = app.firestore();
      const data = await db.collection("Boards").get();
      setBoards(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
    };
    fetchData();
  },[]);

    const [user, setUser] = React.useState([]);
  
  React.useEffect(() => {
    const fetchData = async () => {
      const db = app.firestore();
      const data = await db.collection("users_").get();
      setUser(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
    };
    fetchData();
  }, []);

 let mam = []

// boards.map((e) => 
//       //console.log(e)
//       user.map((ev) => 
//     { if(e.mambers == ev.name){
//           console.log(ev.id)
//         }}
//     )
       
      
//       )


//  for (let i = 0, l = user.length; i < l; i++) {
//   for (let i = 0, l = boards.length; i < l; i++) {
//     if(mam.length != boards.length){
//      boards.map((e) => 
//       //console.log(e)
//       user.map((ev) => 
        
//     { if(e.mambers.map((m) =>
//        m == ev.name) ){
//           console.log(ev.id)
//         }}
//     )
       
      
//       )

//     }
//   }
// }

function onDelete  ( id,dispatch)  {
   
   dispatch(deleteBoard(id))
   
}

//function onDelete (spell)  {
	// console.log(spell.id);
 //  const db = app.firestore();
 //  db.collection('Boards').doc(spell.id).delete().then(function() {
 //        alert("Document successfully deleted!");
 //    }).catch(function(error) {
 //        alert(error.message)
 //    });
    

//}
 


//console.log(boards)




 
  return (
    
    	<div >
		  		  <h1>Boards</h1>

            <p><Button
            type="submit"
            
            variant="contained"
            color="secondary"
            component={Link}
            to="/dashboard">
            
            Back to Dashboard
                </Button></p>
				   
    				  <p><center><Button
    					type="submit"
    					
    					variant="contained"
    					color="primary"
    					component={Link}
    					to="/boards/new">
    					
    					 Create Board
          			</Button> </center></p>

          		
            
          		<div className="row">
                <div className="col-xl-12">
                  {boards.length > 0 && boards.map(spell => (
                    <div
                      key={spell.id}
                      className="card float-left"
                      style={{ width: "18rem", marginRight: "1rem" }}
                    >
                      <div className="card-body">
                        <h5 className="card-title">{spell.name}</h5>
                        <p className="card-text">{spell.description}</p>
                        {spell.user_id === firebase.getCurrentId() &&
                          <div>
                          <p><button
                            className="btn btn-sm btn-danger "
                            
                            onClick={() => onDelete(spell.id,dispatch)}
                          >
                            Delete
                          </button></p>
                          </div>
                          }
                          <Example1 spell={spell} />
                          
                        
                     
                      </div>
                    </div>
                  ))}
                </div>
              </div>
					  
		</div>
    
      
  );
}






function Example1({ spell }) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

 

  return (
    <div>      <Button  class="btn btn-info btn-sm" variant="primary" onClick={handleShow}>
        show Mambers
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>List Of Mambers</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <h5>	{spell.mambers.map(home => <div>{home.label}</div>)}</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default Board;



