import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../Reducers';
//enable redux extension
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


//combine reducer, middlewares and redux chrome extension
//const store = createStore(todoDispatcher, composeEnhancers(applyMiddleware(thunk)))
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

export default store;