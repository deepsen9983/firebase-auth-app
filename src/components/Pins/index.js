import React , { useState }from 'react'

import { Button } from '@material-ui/core'
import withStyles from '@material-ui/core/styles/withStyles'
import { Link , withRouter ,useHistory} from 'react-router-dom'

import Modal from "react-bootstrap/Modal";
import "bootstrap/dist/css/bootstrap.min.css";
import Form from "react-bootstrap/Form";
import AddCircleIcon from '@material-ui/icons/AddCircle';
import app from 'firebase/app'
//import './index1.css'
import AddPin from '../Pins/add.js'

import AddIcon from '@material-ui/icons/Add';
import { SpellInput } from "./edit";
import firebase from '../firebase'
//import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';



function Pins(props) {
	
  const [pins, setPins] = React.useState([]);

  if(!firebase.getCurrentUsername()) {
    // not logged in
    alert('Please login first')
    props.history.replace('/login')
    return null
  }

  React.useEffect(() => {
    let isSubscribed = true
    const fetchData = async () => {
      const db = app.firestore();
      const data = await db.collection("Pins").get();
      if (isSubscribed) {
      setPins(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));}
    };
    fetchData();
    return () => isSubscribed = false
  },[]);
 


  return (
    
    <div >
		  		  <h1>Pins</h1>

            <Button
              type="submit"
              
              variant="contained"
              color="secondary"
              component={Link}
              to="/dashboard">
              
              Back to Dashboard
            </Button>

            <p><div class="row">
               <div class="column">
                  <div class="card">
                    <Button
                      variant="contained"
                      color="primary"
                      type="submit"
                      component={Link}
                      to="/boards"
                      startIcon={<AddIcon />}
                    >
                      ADD Snippet
                    </Button>
                  </div>
                </div>
                  
                  <div class="column">
                    <div class="card">
                      <Button 
                      variant="contained"
                      color="primary"
                      startIcon={<AddIcon />}
                      component={Link}
                      to="/pins/add">
                      add component
                     </Button>
                    </div>
                  </div>
                  
                  
                <div class="column">
                  <div class="card">
                    <Button
                      variant="contained"
                      color="primary"
                      component={Link}
                      to="/tags"
                      startIcon={<AddIcon />}
                     >
                      ADD Task List
                    </Button>
                  </div>
                </div>
                  
                <div class="column">
                  <div class="card">
                    <Button
                      variant="contained"
                      color="primary"
                      component={Link}
                      to="/notes"
                      startIcon={<AddIcon />}
                    >
                      ADD Note
                    </Button>
                  </div>
                </div>
            </div></p>
             
         
         
          <div className="row">
                <div className="col-xl-12">
                  {pins.length > 0 && pins.map(spell => (
                    <div
                      key={spell.id}
                      className="card float-left"
                      style={{ width: "18rem", marginRight: "1rem" }}
                    >
                      <div className="card-body">
                        <h5 className="card-title">{spell.title}</h5>
                        
                          <SpellInput spell={spell} />
                          
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
       				 
      		</div>
    
      
  );
}







export default Pins; 



