import React, { useState } from 'react'
import { Typography, Paper, Avatar, Button, FormControl, Input, InputLabel } from '@material-ui/core'
import withStyles from '@material-ui/core/styles/withStyles'
import { Link, withRouter } from 'react-router-dom'
import firebase from '../firebase'
import AddCircleIcon from '@material-ui/icons/AddCircle';
import DropzoneAreaExample from '../Pins/upload.js'
import app from 'firebase/app'
import Select from 'react-select';
import { useDispatch } from 'react-redux';
import { addPin } from '../Actions/action';

// import Autocomplete from '@material-ui/lab/Autocomplete';
// import { makeStyles } from '@material-ui/core/styles';
// import TextField from '@material-ui/core/TextField';



const styles = theme => ({
	main: {
		width: 'auto',
		display: 'block', // Fix IE 11 issue.
		marginLeft: theme.spacing.unit * 3,
		marginRight: theme.spacing.unit * 3,
		[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
			width: 400,
			marginLeft: 'auto',
			marginRight: 'auto',
		},
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
	},
	avatar: {
		margin: theme.spacing.unit,
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing.unit,
	},
	submit: {
		marginTop: theme.spacing.unit * 3,
	},
})



function AddPin(props) {
	const { classes } = props


	const [board, setBoards] = React.useState([]);
	const [user, setUser] = React.useState([]);
	const [teg, setTeg] = React.useState([]);
	var user_id = firebase.getCurrentId()
	const dispatch = useDispatch();


	// React.useEffect(() => {
	//     const fetchData = async () => {
	//       const db = app.firestore();
	//       const data = await db.collection("users_").get();
	//       setUser(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
	//     };
	//     fetchData();
	// }, []);

	// let users = []

	//  for (let i = 0, l = user.length; i < l; i++) {
	//  	if(users.length != user.length){
	//  	 user.map((e) => 
	//  	 	//console.log(e)
	//  	 	users.push({label: e.name, value: e.id})
	//  	  )
	//  	}
	//   }
 
	
	React.useEffect(() => {
	    const fetchData = async () => {
	      const db = app.firestore();
	      const data = await db.collection("Boards").get();
	      setBoards(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
	    };
	    fetchData();
	}, []);

	let boards = []

	 for (let i = 0, l = board.length; i < l; i++) {
	 	if(boards.length != board.length){
	 	 board.map((e) => 
	 	 	//console.log(e)
	 	 	boards.push({label: e.name, value: e.id,user: e.mambers})
	 	  )
	 	}
	  }

// console.log(boards)

//   for (let i = 0, l = user.length; i < l; i++) {
//   for (let i = 0, l = board.length; i < l; i++) {
//     if(mam.length != board.length){
//      board.map((e) => 
//       //console.log(e)
//       user.map((ev) => 
        
//     { if(e.mambers.map((m) =>
//        m == ev.name) ){
//           console.log(ev.id)
//         }}
//     )
       
      
//       )

//     }
//   }
// }


 React.useEffect(() => {
    const fetchData = async () => {
      const db = app.firestore();
      const data = await db.collection("Tegs").get();
      setTeg(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
    };
    fetchData();
  }, []);

 let tegs = []
console.log(teg)
 for (let i = 0, l = teg.length; i < l; i++) {
 	if(tegs.length != teg.length){
 	 teg.map((e) => 
 	 	//console.log(e)
 	 	tegs.push({label: e.name, value: e.id})
 	  )
 	}
  }

  console.log(tegs)

    const [title, setTitle] = useState('')
	const [content, setContent] = useState('')
	const [ptag, setPtag] = useState([]);
	const [pboard, setPboard] = useState('');
	const [pmamber, setPmamber] = useState([]);
	const [pmambers_id, setPmambers_id] = useState([]);
	const [users, setUsers] = useState([]);

	// console.log(users)
	//  console.log(tegs)
	// console.log(boards)

	const handleChange = (e) => {
   	
   	  console.log('========================handleChange=========1========')
   	  console.log(e)
   	  console.log('========================handleChange=========2========')
      //console.log(e)

      const sportsData: string[] = 
      e.map((ev) => ev.label || ev)
      console.log(sportsData);

      const mambers_id: string[] = 
      e.map((ev) =>  ev.value || ev);
      console.log(mambers_id)

      // { e && e.map(item => {
      //     return <div key={item.value}>{item.label} || item </div>;
      //   })}

       setPmamber(e);
       setPmambers_id(mambers_id);
      //setMambers(event.target.value);
      
    }

    const handleChanget = (e) => {
   	
   	  console.log('========================handleChange=========1========')
   	  console.log(e)

   	  console.log('========================handleChange=========2========')
      //console.log(e)

      // const sportsData: string[] = 
      // e.map((ev) => ev.label || ev)
      // console.log(sportsData)

      // { e && e.map(item => {
      //     return <div key={item.value}>{item.label} || item </div>;
      //   })}

   	  // console.log('========================handleChange==========3=======')

      setPtag(e);

      //setMambers(event.target.value);
      
    }
    //let sportsData=[]  
    const handleChangeb = (e) => {
   	
   	  console.log('========================handleChange=========1========')
   	  console.log(e)

   	  console.log('========================handleChange=========2========')
      console.log(e.user)

      const sportsData = []  
       sportsData.push({label: e.label , value: e.value }) 
     // console.log(sportsData);
      setPboard(sportsData);
      // 	const data: string[] = e.user
     
      // console.log(data)
  		 setUsers(e.user);
    //    // sportsData.push(e.user) 
    //    //e.map((ev) => ev.label || ev)
       
        // console.log(users)

      // { e && e.map(item => {
      //     return <div key={item.value}>{item.label} || item </div>;
      //   })}

   	  // console.log('========================handleChange==========3=======')

      
      //setMambers(event.target.value);
      
    }

    console.log(users)

    function onCreate (title: title ,content: content,ptag: ptag,pboard: pboard,pmamber: pmamber ,pmambers_id: pmambers_id,user_id: user_id,dispatch)  {
	  try {
	    dispatch(addPin(title: title ,content: content,ptag: ptag,pboard: pboard,pmamber: pmamber ,pmambers_id: pmambers_id,user_id: user_id))   
        props.history.replace('/pins')
	   } catch(error) {
		  alert(error.message)
		}
     
    }

	
		
    return (
    
    	<main className={classes.main}>
			<Paper className={classes.paper}>
				<Avatar className={classes.avatar}>
					<AddCircleIcon />
				</Avatar>
				<Typography component="h1" variant="h5">
					Add Component
       			</Typography>
				<form className={classes.form} onSubmit={e => e.preventDefault() && false }>
					<FormControl margin="normal" required fullWidth>
						<InputLabel htmlFor="title">Pin Title</InputLabel>
						<Input id="title" name="title" autoComplete="on" autoFocus value={title} onChange={e => setTitle(e.target.value)} />
					</FormControl>
					<FormControl margin="normal" required fullWidth>
						<InputLabel htmlFor="content">Pin Content</InputLabel>
						<Input  name="content" type="text" autoComplete="off" value={content} onChange={e => setContent(e.target.value)}  />
					</FormControl>

					 <Select
			            placeholder="Tegs"
			            onChange={handleChanget}   
			            autoFocus
			            noOptionsMessage={() => 'no other Tegs :('}
			              isMulti
			              isSearchable
			              options={tegs}
			          />
					

				    <Select
					  placeholder="boards"
					  onChange={handleChangeb}	 
					  autoFocus
					  
				      isSearchable
				      options={boards}
				    />

					<Select
					  placeholder="contributors"
					  onChange={handleChange} 
					  autoFocus
					  noOptionsMessage={() => 'no other mambers :('}
				      isMulti
				      isSearchable
				      options={users}
				    />

					

			     
					
					<DropzoneAreaExample />

					<Button
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						onClick={() => onCreate(title: title ,content: content,ptag: ptag,pboard: pboard ,pmamber: pmamber ,pmambers_id: pmambers_id ,user_id: user_id,dispatch)}
						className={classes.submit}>
						Save
          			</Button>

					<Button
						type="submit"
						fullWidth
						variant="contained"
						color="secondary"
						component={Link}
						to="/pins"
						className={classes.submit}>
						Cancel
          			</Button>
				</form>
			</Paper>
		</main>
    
      
  	)

  	
}

export default withRouter(withStyles(styles)(AddPin)) 







