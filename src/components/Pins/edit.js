import React , { useState }from "react";
import firebase from '../firebase'
import app from 'firebase/app'
import Modal from "react-bootstrap/Modal";
import {  Button } from '@material-ui/core'
import "bootstrap/dist/css/bootstrap.min.css";
import Select from 'react-select';
import './index1.css'
import MenuItem from '@material-ui/core/MenuItem';
import { useDispatch } from 'react-redux';
import { updatePin ,deletePin} from '../Actions/action';

export const SpellInput = ({ spell }) => {
  const dispatch = useDispatch();

  function onDelete  (id,dispatch)  {
    dispatch(deletePin( id ))
  }

  return (

    <div>
       {(spell.pmambers_id && spell.pmambers_id.some(v =>(v === firebase.getCurrentId())) || spell.user_id === firebase.getCurrentId())  &&                               
      
        <div>
         <p><Example1 spell={spell}/></p>
         <div>
            <p><button
            className="btn btn-sm btn-danger "
                            
            onClick={() => onDelete(spell.id,dispatch)}
            >
            Delete
            </button></p>
         </div>
        </div>
      }

      <Example spell={spell}/>
      
     
    </div>
  );
};

function Example1({ spell }) {
  const [show, setShow] = useState(false);
  const [board, setBoards] = React.useState([]);
  const [user, setUser] = React.useState([]);
  const [teg, setTeg] = React.useState([]);
  var user_id = firebase.getCurrentId()
  const dispatch = useDispatch();


  React.useEffect(() => {
    const fetchData = async () => {
      const db = app.firestore();
      const data = await db.collection("users_").get();
      setUser(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
    };
    fetchData();
  }, []);

let users = []

 for (let i = 0, l = user.length; i < l; i++) {
  if(users.length != user.length){
   user.map((e) => 
    //console.log(e)
    users.push({label: e.name, value: e.id})
    )
  }
  }
 
  
  React.useEffect(() => {
    const fetchData = async () => {
      const db = app.firestore();
      const data = await db.collection("Boards").get();
      setBoards(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
    };
    fetchData();
  }, []);

let boards = []

 for (let i = 0, l = board.length; i < l; i++) {
  if(boards.length != board.length){
   board.map((e) => 
    //console.log(e)
    boards.push({label: e.name, value: e.id})
    )
  }
  }





 React.useEffect(() => {
    const fetchData = async () => {
      const db = app.firestore();
      const data = await db.collection("Tegs").get();
      setTeg(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
    };
    fetchData();
  }, []);

 let tegs = []

 for (let i = 0, l = teg.length; i < l; i++) {
  if(tegs.length != teg.length){
   teg.map((e) => 
    //console.log(e)
    tegs.push({label: e.name, value: e.id})
    )
  }
  }


  const [title, setTitle] = React.useState(spell.title);
  const [content, setContent] = React.useState(spell.content);
  const [ptag, setPtag] = React.useState(spell.ptag);
  const [pboard, setPboard] = React.useState(spell.pboard);
  const [pmamber, setPmamber] =React.useState(spell.pmamber);
  const [pmambers_id, setPmambers_id] =React.useState(spell.pmambers_id);
  

//console.log(ptag)


  const handleChange = (e) => {
      const sportsData: string[] = 
      e.map((ev) => ev.label || ev)
      console.log(sportsData)
      const mambers_id: string[] = 
      e.map((ev) =>  ev.value || ev);
      console.log(mambers_id)

      // { e && e.map(item => {
      //     return <div key={item.value}>{item.label} || item </div>;
      //   })}

      setPmamber(sportsData);
      setPmambers_id(mambers_id);
      
  }

    const handleChanget = (e) => {
      const sportsData: string[] = 
      e.map((ev) => ev.label || ev)
      console.log(sportsData)
      setPtag(sportsData);
    }

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  function onUpdate (title: title ,content: content,ptag: ptag,pboard: pboard,pmamber: pmamber ,pmambers_id: pmambers_id,id ,user_id: user_id,dispatch)  {
    dispatch(updatePin(title: title ,content: content,ptag: ptag,pboard: pboard,pmamber: pmamber ,pmambers_id: pmambers_id,id ,user_id: user_id))
    setShow(false)
    //store.dispatch({type: 'UPDATE_PIN',title: title ,content: content,ptag: ptag,pboard: pboard,pmamber: pmamber ,user_id: firebase.getCurrentId(),id: id }).then(setShow(false))
  }
 

  return (
    <div>
      <Button  class="btn btn-info btn-sm" variant="primary" onClick={handleShow}>
        Edit
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Pins</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input value={title} onChange={e => { setTitle(e.target.value);}} /><br/>
          <input value={content} onChange={e => { setContent(e.target.value);}} /><br/>
          <Select
              placeholder="Tags"
              onChange={handleChanget}   
              autoFocus
              noOptionsMessage={() => 'no other Tags :('}
              isMulti
              isSearchable
              options={tegs}
              value={ptag}
           

            />

             
                 


            <Select
              placeholder="boards"
              onChange={setPboard}   
              autoFocus
              value={pboard}
              isSearchable
              options={boards}
            />

          <Select
            placeholder="contributors"
            onChange={handleChange} 
            autoFocus
            noOptionsMessage={() => 'no other mambers :('}
            isMulti
            isSearchable
            options={users}
            value={pmamber}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>

          <Button variant="secondary" onClick={() => onUpdate(title: title ,content: content,ptag: ptag,pboard: pboard,pmamber: pmamber,pmambers_id: pmambers_id, spell.id,user_id: user_id,dispatch)}>
            save
          </Button>
          
        </Modal.Footer>
      </Modal>
    </div>
  );
}

function Example({ spell }) {
//   const [board, setBoards] = React.useState([]);
//   const [user, setUser] = React.useState([]);
//   const [teg, setTeg] = React.useState([]);
   const [show, setShow] = useState(false);


//   React.useEffect(() => {
//     const fetchData = async () => {
//       const db = app.firestore();
//       const data = await db.collection("users_").get();
//       setUser(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
//     };
//     fetchData();
//   }, []);

// let users = []

//  for (let i = 0, l = user.length; i < l; i++) {
//   if(users.length != user.length){
//    user.map((e) => 
//     //console.log(e)
//     users.push({label: e.name, value: e.id})
//     )
//   }
//   }
 
  
//   React.useEffect(() => {
//     const fetchData = async () => {
//       const db = app.firestore();
//       const data = await db.collection("Boards").get();
//       setBoards(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
//     };
//     fetchData();
//   }, []);

// let boards = []

//  for (let i = 0, l = board.length; i < l; i++) {
//   if(boards.length != board.length){
//    board.map((e) => 
//     //console.log(e)
//     boards.push({label: e.name, value: e.id})
//     )
//   }
//   }


//  React.useEffect(() => {
//     const fetchData = async () => {
//       const db = app.firestore();
//       const data = await db.collection("Tegs").get();
//       setTeg(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
//     };
//     fetchData();
//   }, []);

//  let tegs = []

//  for (let i = 0, l = teg.length; i < l; i++) {
//   if(tegs.length != teg.length){
//    teg.map((e) => 
//     //console.log(e)
//     tegs.push({label: e.name, value: e.id})
//     )
//   }
//   }


  const [title, setTitle] = React.useState(spell.title);
  const [content, setContent] = React.useState(spell.content);
  const [ptag, setPtag] = React.useState(spell.ptag);
  const [pboard, setPboard] = React.useState(spell.pboard);
  const [pmamber, setPmamber] =React.useState(spell.pmamber);
 
  
  // let s = pboard.value
  // console.log(s)
// let mam = []
//    for (let i = 0, l = user.length; i < l; i++) {
//    for (let i = 0, l = board.length; i < l; i++) {
//      if(mam.length != board.length){
//       board.map((e) => 
//        //console.log(e)
//       {if (s == e.id){
//        user.map((ev) => 
        
//      { if(e.mambers.map((m) =>
//         m == ev.name) ){
//            console.log(ev.id)
//          }}
//      )}}
       
      
//        )

//      }
//    }
//  }




  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  

  return (
    <div>
      <Button  class="btn btn-info btn-sm" variant="primary" onClick={handleShow}>
        show
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Show Pins</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <center><h1>{spell.title}</h1></center>
         <hr class="new5" />
         <h6>content : {spell.content}</h6>
        <h3> Board Name: {pboard.map(home => <div>{home.label}</div>)}</h3>
        <h5><b>contributors:</b> {pmamber.map(mamber => (mamber.label+", "))}</h5>
        <h5 > <b>Tags :</b> {ptag.map(tag => (tag.label))}</h5> 
    
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>

        </Modal.Footer>
      </Modal>
    </div>
  );
}
