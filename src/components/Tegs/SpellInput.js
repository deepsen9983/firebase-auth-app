import React , { useState }from "react";
import firebase from '../firebase'
import app from 'firebase/app'
import Modal from "react-bootstrap/Modal";
import {  Button } from '@material-ui/core'
import "bootstrap/dist/css/bootstrap.min.css";
import { useDispatch } from 'react-redux';
import { deleteTag ,updateTag } from '../Actions/action';



export const SpellInput = ({ spell }) => {
 const dispatch = useDispatch();

 function onDelete  (id,dispatch)  {
   dispatch(deleteTag(id))
 }

 
  return (
    <div>
      {spell.user_id === firebase.getCurrentId() &&
        <div>
         <p><Example1 spell={spell}/></p>
         <button className="btn btn-sm btn-danger " onClick={() => onDelete(spell.id,dispatch)}>Delete</button>
        </div>
      }
    </div>
  );
};

function Example1({ spell }) {
  const [name, setName] = React.useState(spell.name);
  const [show, setShow] = useState(false);
  var user_id = firebase.getCurrentId()
  const dispatch = useDispatch();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  function onUpdate (name: name ,user_id: user_id,id: id ,dispatch )  {
    dispatch(updateTag(name: name ,user_id: user_id, id: id )) 
    setShow(false)  
    //store.dispatch({type: 'UPDATE_TAG', name: name ,user_id: firebase.getCurrentId(),id: id }).then(setShow(false))
  
  }
  // const onUpdate = () => {
  //   const db = app.firestore()
  //   db.collection('Tegs').doc(spell.id).set({...spell, name})
  //   .then(function() {
  //       alert("Document successfully Update!");
  //       setShow(false);
  //   }).catch(function(error) {
  //       alert(error.message)
  //   });
  // }

  return (
    <div>
      <Button  class="btn btn-info btn-sm" variant="primary" onClick={handleShow}>
        Update
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Tegs</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input
        value={name}
        onChange={e => {
          setName(e.target.value);
        }}
      />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>

          <Button variant="secondary" onClick={() => onUpdate(name: name ,user_id: user_id, spell.id,dispatch)}>
            save
          </Button>
          
        </Modal.Footer>
      </Modal>
    </div>
  );
}
