import React, { useState } from 'react'
import { Typography, Paper, Avatar, Button, FormControl, Input, InputLabel } from '@material-ui/core'

import withStyles from '@material-ui/core/styles/withStyles'
import { Link, withRouter } from 'react-router-dom'
import Modal from "react-bootstrap/Modal";

import AddCircleIcon from '@material-ui/icons/AddCircle';
import { useDispatch } from 'react-redux';
import { addTag } from '../Actions/action';
import firebase from '../firebase'
import app from 'firebase/app'
import { SpellInput } from "./SpellInput";




const styles = theme => ({
	main: {
		width: 'auto',
		display: 'block', // Fix IE 11 issue.
		marginLeft: theme.spacing.unit * 3,
		marginRight: theme.spacing.unit * 3,
		[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
			width: 400,
			marginLeft: 'auto',
			marginRight: 'auto',
		},
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
	},
	avatar: {
		margin: theme.spacing.unit,
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing.unit,
	},
	submit: {
		marginTop: theme.spacing.unit * 3,
	},
})




function Tegs(props) {
	const { classes } = props
	

	if(!firebase.getCurrentUsername()) {
	    // not logged in
	    alert('Please login first')
	    props.history.replace('/login')
	    return null
	}

	const [spells, setSpells] = React.useState([]);
	var user_id = firebase.getCurrentId()
  
	
	React.useEffect(() => {
		//const abortController = new AbortController();
		//const signal = abortController.signal
		let isSubscribed = true

	    const fetchData = async () => {
	      const db = app.firestore();
	      const data = await db.collection("Tegs").get();
	      if (isSubscribed) {
	        
	      
	      setSpells(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));}
	    };
	    fetchData();

	    // return function cleanup(){
	    // 	abortController.abort()
	    // }
	    return () => isSubscribed = false
  },[]);


  return (
    	<div>
    	 <Button
            type="submit"
            
            variant="contained"
            color="secondary"
            component={Link}
            to="/dashboard">
            
            Back to Dashboard
                </Button>
        <p><main className={classes.main}>
			<Paper className={classes.paper}>
				<Avatar className={classes.avatar}>
					<AddCircleIcon />
				</Avatar>
				
				<Example1 />
			</Paper>
		  </main></p>

		<div className="row">
            <div className="col-xl-12">
                {spells.length > 0 && spells.map(spell => (
                    <div
                      key={spell.id}
                      className="card float-left"
                      style={{ width: "18rem", marginRight: "1rem" }}
                    >
                      <div className="card-body">
                        <h5 className="card-title">{spell.name}</h5>
                        <SpellInput spell={spell} />
                      </div>
                    </div>
                  ))}
                </div>
              </div>
			
		</div>
    )

  	
}

function Example1(props) {
	
  const [show, setShow] = useState(false);
  var user_id = firebase.getCurrentId()
  const [name, setName] = React.useState('');
  const dispatch = useDispatch();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  function onCreate (name: name ,user_id: user_id,dispatch)  {
	dispatch(addTag(name: name ,user_id: user_id))
	setShow(false)     
   // store.dispatch({type: 'CREATE_TAG', name: name  ,user_id: user_id}).then(setShow(false))
	
  }

//  const onCreate = () => {
	          
// 	    try {
// 			const db = app.firestore();
			
// 			 db.collection("Tegs").add({ name: name , user_id: user_id});
// 			 setShow(false);
// 			} catch(error) {
// 				alert(error.message)
// 			}
// }

  return (
    <div>
      <Button  class="btn btn-info btn-sm" variant="primary" onClick={handleShow}>
        Create Tags
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Create Tags</Modal.Title>
        </Modal.Header>
        <Modal.Body>
         <InputLabel htmlFor="name"> Name</InputLabel>
		 <Input id="name" name="name" autoComplete="off" autoFocus value={name} onChange={e => setName(e.target.value)} />
				
		</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>

          <Button variant="secondary" onClick={() => onCreate(name: name ,user_id: user_id,dispatch)} >
            save
          </Button>
          
        </Modal.Footer>
      </Modal>
    </div>
  );
}



export default withRouter(withStyles(styles)(Tegs)) 







