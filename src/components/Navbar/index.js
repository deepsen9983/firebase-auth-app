import React from 'react';
import { makeStyles } from "@material-ui/core/styles"

//import HomePage from '../HomePage'
//import firebase from '../firebase'

//import Dashboard from '../Dashboard'
//import {  Button } from '@material-ui/core'

import './index.css'

import {
  BrowserRouter as Router,
  Switch, Route, Link
} from "react-router-dom";

import {
  Drawer, List, ListItem,
  ListItemIcon, ListItemText,
  Container, Typography,
} from "@material-ui/core";

import HomeIcon from "@material-ui/icons/Home";
import InfoIcon from '@material-ui/icons/Info';
//import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const useStyles = makeStyles((theme) => ({
  drawerPaper: { width: 'inherit' },
  link: {
    textDecoration: 'none',
    
    color: theme.palette.text.primary
  }
}))

function Navbar(props) {
  const classes = useStyles();
  return (
    <Router>
      <div style={{ display: 'flex' }}>
        <Drawer
          style={{ width: '50px' }}
          variant="persistent"
          anchor="left"
          open={true}
          classes={{ paper: classes.drawerPaper }}
        >
     <List  id='nav'>
            <Link to="/" className={classes.link}>
              <ListItem button>
                <ListItemIcon>
                  <HomeIcon />
                </ListItemIcon>
                
              </ListItem>
            </Link>
            <Link to="/about" className={classes.link}>
              <ListItem button>
                <ListItemIcon>
                  <InfoIcon />
                </ListItemIcon>
                
              </ListItem>
            </Link>
          </List>
        </Drawer>
        <Switch>
          <Route exact path="/">
            
              <Typography variant="h3" gutterBottom>
                Home
              </Typography>
              
            
          </Route>
          <Route exact path="/about">
            
              <Typography variant="h3" gutterBottom>
                About
              </Typography>
              
          </Route>
        </Switch>
      </div>
    </Router>
  );
  
}

export default Navbar;