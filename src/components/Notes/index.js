import React, { useState } from 'react'
import { Typography, Paper, Avatar, Button, FormControl, Input, InputLabel } from '@material-ui/core'

import withStyles from '@material-ui/core/styles/withStyles'
import { Link, withRouter } from 'react-router-dom'
import Modal from "react-bootstrap/Modal";

import AddCircleIcon from '@material-ui/icons/AddCircle';

import firebase from '../firebase'
import app from 'firebase/app'
//import { SpellInput } from "./SpellInput";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import {useSelectore} from 'react-redux'
import { useDispatch } from 'react-redux';

import { deleteNote , addNote } from '../Actions/action';


const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));



const styles = theme => ({
	main: {
		width: 'auto',
		display: 'block', // Fix IE 11 issue.
		marginLeft: theme.spacing.unit * 3,
		marginRight: theme.spacing.unit * 3,
		[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
			width: 400,
			marginLeft: 'auto',
			marginRight: 'auto',
		},
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
	},
	avatar: {
		margin: theme.spacing.unit,
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing.unit,
	},
	submit: {
		marginTop: theme.spacing.unit * 3,
	},
})




function Tegs(props) {
	const { classes } = props
	var user_id = firebase.getCurrentId()
	const dispatch = useDispatch();

	if(!firebase.getCurrentUsername()) {
		// not logged in
		alert('Please login first')
		props.history.replace('/login')
		return null
	}

	const [spells, setSpells] = React.useState([]);
  
	
	React.useEffect(() => {
    const fetchData = async () => {
      const db = app.firestore();
      const data = await db.collection("Notes").get();
      setSpells(data.docs.map(doc => ({ ...doc.data(), id: doc.id })));
    };
    fetchData();
  },[]);


// const handleDelete = (props, id) => {
//     props.deleteToDo(id)
// }
function onDelete  ( id,dispatch)  {
   // store.dispatch({type: 'DELETE_NOTE', id:id, })
   dispatch(deleteNote(id))
   
}

//const conter = useSelectore(state => state);
  return (
    	<div>
    	 <Button
            type="submit"
            
            variant="contained"
            color="secondary"
            component={Link}
            to="/dashboard">
            
            Back to Dashboard
                </Button>
        <p><main className={classes.main}>
			<Paper className={classes.paper}>
				<Avatar className={classes.avatar}>
					<AddCircleIcon />
				</Avatar>
				
				<Example1 />
			</Paper>
		  </main></p>

		<div>
	      <table className="table" >
	        <thead>
	          <tr>
	            <th>Note_Name</th>

	            <th>Notes </th>
	            <th>Actions </th>
	          </tr>
	        </thead>
	        <tbody>
	          {spells.map(note => (
	            <tr>
	              <td> {note.name} </td>
	              <td className = "text-wrap text-justify"> {note.note}</td>
	              {note.user_id === firebase.getCurrentId() &&
	              <td><button
                        className="btn btn-sm btn-danger "
                         onClick={() => onDelete( note.id,dispatch)}
                      
                        >
                        Delete
                        </button>
                  </td>}

	            </tr>
	          ))}
	        </tbody>
	      </table>
	    </div>
			
		</div>
    )

  	
}

function Example1(props) {
	
  const [show, setShow] = useState(false);
  var user_id = firebase.getCurrentId()
  const [name, setName] = React.useState('');
  const [note, setNote] = React.useState('');
  const dispatch = useDispatch();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  

 function onCreate (name: name , note: note,dispatch)  {

	dispatch(addNote(name: name , note: note))
	setShow(false)
 }

  return (
    <div>
      <Button  class="btn btn-info btn-sm" variant="primary" onClick={handleShow}>
        Create Notes
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Create Tags</Modal.Title>
        </Modal.Header>
        <Modal.Body>
         <InputLabel htmlFor="name"> Note_Name</InputLabel>
		 <Input id="name" name="name" autoComplete="off" autoFocus  onChange={e => setName(e.target.value)} /><br/>
		 
		  <TextField
	          id="standard-multiline-flexible"
	          label="Write Yours Notes "
	          multiline
	          rowsMax={4}
	          
	          onChange={e => setNote(e.target.value)}
	       />	
		</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>

          <Button variant="secondary" onClick={() => onCreate(name: name ,note: note ,dispatch)} >
            save
          </Button>
          
        </Modal.Footer>
      </Modal>
    </div>
  );
}



export default withRouter(withStyles(styles)(Tegs)) 







