import React, { useEffect, useState } from 'react'
import { Typography, Paper, Avatar, CircularProgress, Button } from '@material-ui/core'
import VerifiedUserOutlined from '@material-ui/icons/VerifiedUserOutlined'
import withStyles from '@material-ui/core/styles/withStyles'
import firebase from '../firebase'
import { withRouter ,Link  } from 'react-router-dom'
import './index.css'
import app from 'firebase/app'
import Chart from './Chart.js'
import moment from 'moment'
import Navbar from '../Navbar'
import ComputerIcon from '@material-ui/icons/Computer';
import PinterestIcon from '@material-ui/icons/Pinterest';
import ViewCompactTwoToneIcon from '@material-ui/icons/ViewCompactTwoTone';
import ListAltTwoToneIcon from '@material-ui/icons/ListAltTwoTone';
import NoteAddIcon from '@material-ui/icons/NoteAdd';

const styles = theme => ({
	main: {
		width: 'auto',
		display: 'block', // Fix IE 11 issue.
		marginLeft: theme.spacing.unit * 3,
		marginRight: theme.spacing.unit * 3,
		[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
			width: 400,
			height: 200,

			marginLeft: 'auto',
			marginRight: 'left',
		},
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
	},
	avatar: {
		margin: theme.spacing.unit,
		backgroundColor: theme.palette.secondary.main,
	},
	submit: {
		marginTop: theme.spacing.unit * 3,
	},
})



function Dashboard(props) {
	const { classes } = props

	if(!firebase.getCurrentUsername()) {
		// not logged in
		alert('Please login first')
		props.history.replace('/login')
		return null
	}

	const [quote, setQuote] = useState('')
	const [bcount, setBcount] = useState(0)

	const db = app.firestore();

	db.collection('Boards').get().then(
  (snapshot) => { console.log(snapshot.docs.length);
  		setBcount(snapshot.docs.length)
  }
);

	useEffect(() => {
		firebase.getCurrentUserQuote().then(setQuote)
	})

	return (
		<div>
		  <div id="chart"><Chart/></div>
			<main className={classes.main}>
				
				<Paper className={classes.paper}>
					<Avatar className={classes.avatar}>
						<VerifiedUserOutlined />
					</Avatar>
				
					<Typography component="h1" variant="h5">
						Hello { firebase.getCurrentUsername() }
					</Typography>
					<Typography component="h1" variant="h5">
						Your quote: {quote ? `"${quote}"` : <CircularProgress size={20} />}
					</Typography>
					<Button
						type="submit"
						fullWidth
						variant="contained"
						color="secondary"
						onClick={logout}
						className={classes.submit}>
						Logout
	          		</Button>
				</Paper>
			</main>

			<main className={classes.main}>
				
				<Paper className={classes.paper}>
					<div>
		          <h5 style={{color:"#007bff"}}> Board Status:</h5>
		          <h6>{bcount} Boards</h6>
		          <p>as on {moment().format('ddd MMM DD, YYYY')}</p>
		          <br/>
		          <br/>
		          <Link to='/boards' style = {{marginRight:8,    fontSize: 19}}>Show All Boards</Link>
		         </div>
		        </Paper>
				
			</main>
		
		
		
		    <div class='card1'>
  
			    <h3>Smart Actions</h3>

				<div class="row">
					 <div class="column">
					    <div class="card">
					      <h4>Pin a Code Snippet</h4>
					      <p>< ViewCompactTwoToneIcon color="primary" style={{ fontSize: 100 }} /></p>
					      <Button
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							component={Link}
							to="/boards"
							className={classes.submit}>
							 Boards
		          			</Button>
					    </div>
					  </div>

					  <div class="column">
					    <div class="card">
					      <h4> Pin a Component</h4>
					      <p><  PinterestIcon color="primary" style={{ fontSize: 100 }} /></p>
					       <Button
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							component={Link}
							to="/pins"
							className={classes.submit}>
							 Pins
		          			</Button>
					    </div>
					  </div>
					  
					  <div class="column">
					    <div class="card">
					      <h4>Pin a Task List</h4>
					      <p>< ListAltTwoToneIcon color="primary" style={{ fontSize: 100 }} /></p>
					       <Button
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						component={Link}
						to="/tags"
						className={classes.submit}>
						 Tags
	          			</Button>
					    </div>
					  </div>
					  
					  <div class="column">
					    <div class="card">
					      <h4>Pin a Note</h4>
					      <p>< NoteAddIcon color="primary" style={{ fontSize: 100 }} /></p>
					       <Button
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						component={Link}
						to="/notes"
						className={classes.submit}>
						 Notes
	          			</Button>
					    </div>
					  </div>
				</div>
			</div>
		</div>
	)

	async function logout() {
		await firebase.logout()
		props.history.push('/')
	}
}

export default withRouter(withStyles(styles)(Dashboard))