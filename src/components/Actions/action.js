import app from 'firebase/app'
import firebase from '../firebase'

//Action for add board note in todo Boards
export const login = (email, password) => {
  
    return (dispatch) => {
        try {
       firebase.login(email, password)
      .then((res) => {
            dispatch({type: 'LOGIN'})
        })
    } catch(error) {
      alert(error.message)
    }
         
          
        
    }
}


//Action for add board note in todo Boards
export const addBoard = (name: name , description: description, mambers: mambers, user_id: user_id) => {
  
    return (dispatch) => {
        const db = app.firestore();
            
        db.collection("Boards").add({ name: name , description: description, mambers: mambers, user_id: user_id})
             
        .then((res) => {
            dispatch({type: 'CREATE_BOARD'})
        })
    }
}


//action for delete board from Boards
export const deleteBoard = (id) => {
   
    return (dispatch) => {
        const db = app.firestore();
       
        db.collection('Boards').doc(id).delete()
        .then((res) => dispatch({type: 'DELETE_BOARD', id: id}))
        .then(function() {
            alert("Document successfully deleted!");
        }).catch(function(error) {
            alert(error.message)
        });
    }
}

//Action for add new pin in todo Pins
export const addPin = (title: title ,content: content,ptag: ptag,pboard: pboard,pmamber: pmamber ,pmambers_id: pmambers_id,user_id: user_id) => {
 
    return (dispatch) => {
        const db = app.firestore();
        db.collection("Pins").add({ title: title ,content: content,ptag: ptag,pboard: pboard,pmamber: pmamber ,pmambers_id: pmambers_id,user_id: user_id})
        .then((res) => {
            dispatch({type: 'CREATE_PIN'})
        })
    }
}


//Action for update  tag in todo Pins
export const updatePin = (title: title ,content: content,ptag: ptag,pboard: pboard,pmamber: pmamber ,pmambers_id: pmambers_id, id: id,user_id: user_id) => {

    return (dispatch) => {
        const db = app.firestore()
        db.collection('Pins').doc(id).set({title: title ,content: content,ptag: ptag,pboard: pboard,pmamber: pmamber ,pmambers_id: pmambers_id, id: id,user_id: user_id})
        .then(function() {
            alert("Document successfully Update!");
              
        }).catch(function(error) {
            alert(error.message)
        })
        .then((res) => {
            dispatch({type: 'UPDATE_PIN'})
        })
    }
}

//action for delete tag from Pins
export const deletePin = (id) => {
    return (dispatch) => {
        const db = app.firestore();
        db.collection('Pins').doc(id).delete()
        .then((res) => dispatch({type: 'DELETE_TAG', id: id}))
        .then(function() {
            alert("Document successfully deleted!");
        }).catch(function(error) {
            alert(error.message)
        });
    }
}

//Action for add new tag in  Tags
export const addTag = (name: name , user_id: user_id) => {
  
    return (dispatch) => {
        const db = app.firestore();
        db.collection("Tegs").add({ name: name , user_id: user_id})
        .then((res) => {
            dispatch({type: 'CREATE_TAG'})
        })
    }
}


//Action for update  tag in  Tags
export const updateTag = (name: name ,user_id: user_id, id: id) => {
  
    return (dispatch) => {
        const db = app.firestore()
        db.collection('Tegs').doc(id).set({name: name ,user_id: user_id})
        .then(function() {
            alert("Document successfully Update!");
              
        }).catch(function(error) {
            alert(error.message)
        })
        .then((res) => {
            dispatch({type: 'UPDATE_TAG'})
        })
    }
}

//action for delete tag from Tags
export const deleteTag = (id) => {
    return (dispatch) => {
        const db = app.firestore();
        db.collection('Tegs').doc(id).delete()
        .then((res) => dispatch({type: 'DELETE_TAG', id: id}))
        .then(function() {
            alert("Document successfully deleted!");
        }).catch(function(error) {
            alert(error.message)
        });
    }
}

//Action for add new note in  Notes
export const addNote = (name: name , note: note) => {
  
    return (dispatch) => {
        const db = app.firestore();
            
        db.collection("Notes").add({ name: name , note: note , user_id: firebase.getCurrentId()})
             
        .then((res) => {
            dispatch({type: 'CREATE_NOTE'})
        })
    }
}


//action for delete note from Notes
export const deleteNote = (id) => {
	console.log(id);

    return (dispatch) => {
    	console.log({id});
        const db = app.firestore();
        db.collection('Notes').doc(id).delete()
        .then((res) => dispatch({type: 'DELETE_NOTE', id: id}))
        .then(function() {
            alert("Document successfully deleted!");
        }).catch(function(error) {
            alert(error.message)
        });
    }
}

