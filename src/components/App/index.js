import React, { useState, useEffect } from 'react'
import './styles.css'
import HomePage from '../HomePage'
import Login from '../Login'
import Register from '../Register'
import Dashboard from '../Dashboard'
import Header from "../header";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { CssBaseline, CircularProgress } from '@material-ui/core'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import firebase from '../firebase'
import Board from '../Board'
import NewBoard from '../Board/new.js'
import Pins from '../Pins'
import AddPin from '../Pins/add.js'
import Tegs from '../Tegs'
import Notes from '../Notes'

const theme = createMuiTheme()

export default function App() {

	const [firebaseInitialized, setFirebaseInitialized] = useState(false)

	useEffect(() => {
		firebase.isInitialized().then(val => {
			setFirebaseInitialized(val)
		})
	})


	return firebaseInitialized !== false ? (
		<MuiThemeProvider theme={theme}>
			<CssBaseline />
			<Router>
			
		      <Header />
		      



				<Switch>
					<Route exact path="/" component={HomePage} />
					<Route exact path="/login" component={Login} />
					<Route exact path="/register" component={Register} />
					<Route exact path="/dashboard" component={Dashboard} />
					<Route exact path="/boards" component={Board} />
					<Route exact path="/boards/new" component={NewBoard} />
					<Route exact path="/pins" component={Pins} />
					<Route exact path="/pins/add" component={AddPin} />
					<Route exact path="/tags" component={Tegs} />
					<Route exact path="/notes" component={Notes} />
				</Switch>
				


			</Router>
		</MuiThemeProvider>
	) : <div id="loader"><CircularProgress /></div>
}