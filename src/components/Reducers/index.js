import { combineReducers } from 'redux';
import tagReducer from './tagReducer'; 
import boardReducer from './boardReducer';
import noteReducer from './notesReducer';
import pinReducer from './pinReducer';



const rootReducer = combineReducers({
 
   tagState: tagReducer,
   boardState: boardReducer,
   noteState: noteReducer,
   pinState: pinReducer,
  
});

export default rootReducer;