import app from 'firebase/app'
import 'firebase/auth'
import 'firebase/firebase-firestore'

const config = {
	
    apiKey: "AIzaSyB4skzAOma_ukQNYqBThtXkPyDQWE3PMrQ",
    authDomain: "pin-borad.firebaseapp.com",
    databaseURL: "https://pin-borad.firebaseio.com",
    projectId: "pin-borad",
    storageBucket: "pin-borad.appspot.com",
    messagingSenderId: "139686930974",
    appId: "1:139686930974:web:db110dd0c2e5b4cadf1c0f",
    measurementId: "G-90QLHZX5DT"
}

class Firebase {
	constructor() {
		app.initializeApp(config)
		this.auth = app.auth()
		this.db = app.firestore()
		
	}
 
	login(email, password) {
		return this.auth.signInWithEmailAndPassword(email, password)
	}

	logout() {
		return this.auth.signOut()
	}

	async register(name, email, password) {
		await this.auth.createUserWithEmailAndPassword(email, password)
		return this.auth.currentUser.updateProfile({
			displayName: name
		})
	}

	addQuote(quote) {
		if(!this.auth.currentUser) {
			return alert('Not authorized')
		}

		return this.db.doc(`users_codedamn_video/${this.auth.currentUser.uid}`).set({
			quote
		})
	}




	addUsers(name, email) {
			if(!this.auth.currentUser) {
				return alert('Not authorized')
			}

			return this.db.doc(`users_/${this.auth.currentUser.uid}`).set({
				name, email
			})
		}

		
	isInitialized() {
		return new Promise(resolve => {
			this.auth.onAuthStateChanged(resolve)
		})
	}

	getCurrentUsername() {
		return this.auth.currentUser && this.auth.currentUser.displayName
	}

	getCurrentId() {
		return this.auth.currentUser.uid 
	}


	async getCurrentUserQuote() {
		const quote = await this.db.doc(`users_codedamn_video/${this.auth.currentUser.uid}`).get()
		return quote.get('quote')
	}
}

export default new Firebase()