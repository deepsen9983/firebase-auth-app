import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
//import Application from './Components/Application';
//import signin from './Components/SignIn';
import {createStore, applyMiddleware, compose} from 'redux';
import {Provider} from 'react-redux';
import todoDispatcher from './components/Reducers/notesReducer'
import './index.css';
import store from './components/store';

//const store = createStore(todoDispatcher);

ReactDOM.render(
  <Provider store={store}><App /> </Provider>,
  document.getElementById('root')
);
